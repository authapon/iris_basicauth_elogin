package iris_basicauth_elogin

import (
	"encoding/base64"
	"encoding/json"
	"gopkg.in/kataras/iris.v10"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func Reject(ctx iris.Context) {
	ctx.Header("WWW-Authenticate", "Basic realm="+strconv.Quote("Authorization Required"))
	ctx.StatusCode(iris.StatusUnauthorized)

}

func Serve(ctx iris.Context) {
	authheader := ctx.GetHeader("Authorization")
	if len(authheader) == 0 {
		Reject(ctx)
		return
	}
	dataencode := strings.SplitN(authheader, " ", 2)
	userpass, err := base64.StdEncoding.DecodeString(dataencode[1])
	if err != nil {
		Reject(ctx)
		return
	}
	userpasssp := strings.SplitN(string(userpass), ":", 2)
	user := strings.TrimSpace(userpasssp[0])
	passwd := userpasssp[1]
	rdat := epassport2(user, passwd)
	if rdat["success"].(string) == "false" {
		Reject(ctx)
		return
	}
	ctx.Values().Set("username", user)
	ctx.Values().Set("fullname", rdat["fullname"].(string))
	ctx.Next()
}

func epassport2(username, password string) map[string]interface{} {
	resp, err := http.PostForm("https://elogin2.rmutsv.ac.th", url.Values{"username": {username}, "password": {password}})
	if err != nil {
		edat := make(map[string]interface{})
		edat["success"] = "false"
		return edat
	}
	p := make([]byte, resp.ContentLength)
	resp.Body.Read(p)
	rdat := make(map[string]interface{})
	if err := json.Unmarshal(p, &rdat); err != nil {
		edat := make(map[string]interface{})
		edat["success"] = "false"
		return edat
	}
	return rdat
}
